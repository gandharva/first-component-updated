import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Sub1ComponentComponent } from './sub1-component/sub1-component.component';
import { Sub2ComponentComponent } from './sub2-component/sub2-component.component';

@NgModule({
  declarations: [
    AppComponent,
    Sub1ComponentComponent,
    Sub2ComponentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
