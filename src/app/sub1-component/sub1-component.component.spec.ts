import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sub1ComponentComponent } from './sub1-component.component';

describe('Sub1ComponentComponent', () => {
  let component: Sub1ComponentComponent;
  let fixture: ComponentFixture<Sub1ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sub1ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sub1ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
